import 'package:flutter/material.dart';
import 'package:test_project/models/team_list_response.dart';
import 'package:test_project/presenters/edit_team_presenter.dart';
import 'package:test_project/utilities/my_widget.dart';
import 'package:test_project/utilities/widget/widget_dialog.dart';

class TeamDetailScreen extends StatefulWidget {
  final Teams data;
  TeamDetailScreen(this.data);

  @override
  _TeamDetailScreenState createState() => _TeamDetailScreenState();
}

class _TeamDetailScreenState extends State<TeamDetailScreen>
    with SingleTickerProviderStateMixin {
  _updateTeam() {
    updateTeamsApi(widget.data.idTeam, widget.data.strTeam).then((v) {
      if (v.status.response) {}
    }).catchError((onError) {
      dialogConnError(context);
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: myText(widget.data.strTeam),
      ),
      body: _body(),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Hero(
                tag: widget.data.idTeam,
                child: Image.network(
                  widget.data.strTeamBadge,
                  width: 200.0,
                )),
          ),
          SizedBox(
            height: 8.0,
          ),
          Container(
            child: ListView(
              padding: EdgeInsets.all(10.0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                myText(widget.data.strDescriptionEN),
              ],
            ),
          )
        ],
      ),
    );
  }
}
