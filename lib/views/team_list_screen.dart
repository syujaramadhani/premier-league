import 'package:flutter/material.dart';
import 'package:test_project/models/team_list_response.dart';
import 'package:test_project/presenters/team_presenter.dart';
import 'package:test_project/utilities/my_navigation.dart';
import 'package:test_project/utilities/my_widget.dart';

class TeamListScreen extends StatefulWidget {
  TeamListScreen({Key key}) : super(key: key);

  @override
  TeamListScreenState createState() => TeamListScreenState();
}

class TeamListScreenState extends State<TeamListScreen> {
  List<Teams> listTeams = List<Teams>();
  bool loaded = false;

  _getTeam() {
    getTeamsApi().then((v) {
      setState(() {
        listTeams = v;
        loaded = true;
      });
    });
  }

  @override
  void initState() {
    _getTeam();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: myText("Club List"),
      ),
      body: loaded
          ? _body()
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  Widget _body() {
    return SafeArea(
      child: GridView.builder(
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemBuilder: (c, i) => _itemGrid(listTeams[i]),
        itemCount: listTeams.length,
      ),
    );
  }

  _itemGrid(Teams data) {
    return InkWell(
      onTap: () => MyNavigation.toTeamDetail(context, data),
      child: Card(
        elevation: 4.0,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Hero(
                  tag: data.idTeam,
                  child: Image.network(
                    data.strTeamBadge,
                    width: 50.0,
                  )),
              SizedBox(
                height: 8.0,
              ),
              myText(data.strTeam),
            ],
          ),
        ),
      ),
    );
  }
}
