class UrlStrings {
  static const base_url = "https://www.thesportsdb.com/";
  static const api = "$base_url/api/v1/json/1";
  static const get_team =
      "$api/search_all_teams.php?l=English%20Premier%20League";
  static const get_player = "$api/searchplayers.php?t=arsenal";
}
