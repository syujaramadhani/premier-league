import 'package:flutter/material.dart';
import 'package:test_project/utilities/my_widget.dart';

Widget buttonFlat(text, {textColor, onPressed, padding}) {
  return FlatButton(
      child: myText(text, color: textColor),
      onPressed: onPressed,
      padding: padding);
}
