import 'package:flutter/material.dart';
import 'package:test_project/utilities/my_widget.dart';
import 'package:test_project/utilities/widget/buttons/button_flat.dart';

dialogBasic(title, text, context) {
  showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: myText(
        title,
        fontWeight: FontWeight.w700,
      ),
      content: myText(
        text,
        size: 14.0,
      ),
      actions: <Widget>[
        buttonFlat(
          "OK",
          onPressed: () => Navigator.pop(context, "OK"),
        )
      ],
    ),
  );
}

dialogGalat(text, context) {
  dialogBasic("Perhatian", text, context);
}

dialogConnError(context) {
  dialogGalat("Ada kesalahan, silakan coba beberapa saat lagi.", context);
}
