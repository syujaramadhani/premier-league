import 'package:flutter/material.dart';

Widget myText(text, {size, color, fontWeight}) {
  return Text(
    text,
    style: TextStyle(fontSize: size, color: color, fontWeight: fontWeight),
  );
}
