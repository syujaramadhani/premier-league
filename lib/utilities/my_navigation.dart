import 'package:flutter/material.dart';
import 'package:test_project/views/team_detail_screen.dart';

class MyNavigation {
//  static toHomeNav(context) {
//     Navigator.of(context).pushAndRemoveUntil(
//         MaterialPageRoute(builder: (context) => HomeNavigation()),
//         (Route<dynamic> route) => false);
//   }

  static toTeamDetail(context, data) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => TeamDetailScreen(data)));
  }

  // static toTransfer(context, data) {
  //   Navigator.of(context)
  //       .push(MaterialPageRoute(builder: (context) => TransferScreen(data)));
  // }
}
