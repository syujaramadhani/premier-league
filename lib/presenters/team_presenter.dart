import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:test_project/models/team_list_response.dart';
import 'package:test_project/utilities/url_strings.dart';

Future getTeamsApi() async {
  var response =
      await http.get(UrlStrings.get_team).catchError((err) => print(err));

  try {
    var res = jsonDecode(response.body);
    print(res);
    if (response.statusCode == 200) {
      TeamListResponse response = TeamListResponse.fromJson(res);
      return response.teams;
    } else {
      return response;
    }
  } catch (e) {
    print(e);
    return response;
  }
}
